#! /usr/bin/env python3

import argparse
import logging
import os
import shutil
import subprocess
import traceback

import jinja2

import yaml


THEME_CACHE_PATH = os.path.expanduser('~/.currenttheme')
DOTFILES_DIR = os.path.dirname(os.path.realpath(__file__))


def _abs(path: str):
    return os.path.join(DOTFILES_DIR, path)


def list_available_themes():
    themes_dir = _abs('themes')
    return [file_name.split('.')[0] for file_name in os.listdir(themes_dir)]


def get_args():
    themes = list_available_themes()

    parser = argparse.ArgumentParser()
    parser.add_argument('theme', choices=themes, nargs='?',
                        help='theme to apply')
    parser.add_argument('-m', '--minimal', action='store_true')

    return vars(parser.parse_args())


def fallback_theme():
    """
    Read last used theme from file in case none was specified.
    Defaults to first one available if none was set previously.
    """
    themes = list_available_themes()
    try:
        with open(THEME_CACHE_PATH, 'r') as f:
            theme = f.read().strip()
            assert(theme in themes)
    except (OSError, AssertionError):
        theme = themes[0]
        logging.warning('No theme was specified, and no previous theme '
                        f'was found. Defaulting to \'{theme}\'.')
    return theme


def save_theme_selection(theme):
    with open(THEME_CACHE_PATH, 'w') as f:
        f.write(theme+'\n')


def load_theme(args):
    file_path = os.path.join(DOTFILES_DIR, 'themes', args['theme']+'.yaml')
    with open(file_path, 'r') as theme_file:
        theme = yaml.load(theme_file)
    theme.update(dotfiles_dir=DOTFILES_DIR)
    return theme


def load_config():
    file_path = _abs('config.yaml')
    with open(file_path, 'r') as config_file:
        return yaml.load(config_file)


def log_success(source, destination=None, msg=None):
    if destination:
        print(f'  - \033[32m{source}\033[0m -> {destination}')
    else:
        print(f'  - \033[32m{source}\033[0m')

    if msg:
        print(msg)


def log_failure(source, destination=None, msg=None):
    if destination:
        print(f'  - \033[1;31m{source} -> {destination}\033[0m')
    else:
        print(f'  - \033[1;31m{source}\033[0m')

    if msg:
        print(msg)


def batch(task_func):
    def batch_func(items, *args):

        if isinstance(items, list):
            items = {item: None for item in items}

        for source, dest in items.items():
            full_source = _abs(source)
            if dest:
                full_dest = os.path.expanduser(dest)
            else:
                full_dest = None

            try:
                msg = task_func(full_source, full_dest, *args)
                log_success(source, full_dest, msg)

            except (OSError, AssertionError):
                log_failure(source, full_dest, traceback.format_exc())

    return batch_func


def prepare_path(path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    if os.path.exists(path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


@batch
def copy_files(source, dest):
    prepare_path(dest)
    if os.path.isdir(source):
        shutil.copytree(source, dest)
    else:
        shutil.copy2(source, dest)


@batch
def render_templates(source, dest, theme):
    prepare_path(dest)
    with open(source, 'r') as f:
        jinja2.Template(f.read()).stream(theme).dump(dest)


@batch
def run_scripts(source, _):
    p = subprocess.run(source, stdout=subprocess.PIPE)
    output = p.stdout.decode('utf-8')
    assert p.returncode == 0, output
    return output.strip()


if __name__ == '__main__':
    args = get_args()
    if not args['theme']:
        args['theme'] = fallback_theme()
    save_theme_selection(args['theme'])

    theme = load_theme(args)
    config = load_config()

    print('copying files:')
    copy_files(config['copy'])
    print('rendering templates:')
    render_templates(config['render'], theme)
    print('running scripts:')
    run_scripts(config['run'])
