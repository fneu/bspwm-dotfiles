#! /usr/bin/env python3

import os
import shutil
import subprocess
import sys

theme_file_path = os.path.expanduser('~/.currenttheme')
with open(theme_file_path, 'r') as themefile:
    theme = themefile.read().strip()

active_folder = os.path.expanduser('~/.local/share/TelegramDesktop')
original_folder = os.path.expanduser('~/.local/share/TelegramDesktop_orig')
theme_folder = os.path.expanduser(f'~/.local/share/TelegramDesktop_{theme}')

# Copy to backup folder before messing it up :D
if not os.path.exists(original_folder):
    if not os.path.exists(active_folder):
        sys.exit(1)
    else:
        shutil.copytree(active_folder, original_folder)

# theme folder already exists and is correctly linked
if (os.path.islink(active_folder) and
        os.readlink(active_folder) == theme_folder):
    sys.exit(0)

# we need to change the theme, maybe for the first time
# first kill it if it's running, so that it cannot intervene
tele_pid = subprocess.run(['pgrep', 'telegram'],
                          stdout=subprocess.PIPE).stdout.decode().strip()
if tele_pid:
    subprocess.run(['kill', tele_pid], stdout=subprocess.DEVNULL)

if not os.path.exists(theme_folder):
    shutil.copytree(original_folder, theme_folder)
    print('PLEASE SET THE TELEGRAM THEME MANUALLY THIS TIME')

if os.path.exists(active_folder):
    os.remove(active_folder)

os.symlink(theme_folder, active_folder, target_is_directory=True)

# restart telegram if it was running
if tele_pid:
    subprocess.Popen(['telegram-desktop'],
                     stdout=subprocess.DEVNULL,
                     stderr=subprocess.DEVNULL)
