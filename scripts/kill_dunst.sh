#! /bin/sh

if pgrep -x "dunst" > /dev/null
then
    killall dunst
fi
