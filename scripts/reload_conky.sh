#! /bin/bash

if pgrep -x "conky" > /dev/null
then
    killall -SIGUSR1 conky
fi
