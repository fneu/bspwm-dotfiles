#! /bin/bash

chmod +x \
    ~/.config/bspwm/bspwmrc \
    ~/.config/bspwm/bspwm_colors.sh \
    ~/bin/set_wallpaper \
    ~/bin/lumos \
    ~/bin/nox
