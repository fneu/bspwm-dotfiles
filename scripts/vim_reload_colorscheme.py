#! /usr/bin/python3

import subprocess

# this requires all vim instances to be started with `vim --servername ...`
# vim is thus aliased to some such in .bashrc
vim_query = subprocess.run(['vim', '--serverlist'], stdout=subprocess.PIPE)

# [:-1] removes empty split after the last newline
for vim_name in vim_query.stdout.decode().split('\n')[:-1]:
    subprocess.Popen(
        ['vim',
         '--servername',
         vim_name,
         '--remote-send',
         f'<ESC>:colorscheme colorscheme<CR>'],
        stdout=subprocess.DEVNULL)
