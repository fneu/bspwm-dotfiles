# dotfiles

This is my system configuration. It somewhat themeable by using Jinja2 templates.

# Software and packages

## Repositories

I'm using OpenSUSE Tumbleweed, and some programs are not available at not with the required version in the stock repositories.
Thus, the following should be enabled:

- A repository that provides the proprietary nvidia driver (optional): `download.nvidia.com-tumbleweed         | nVidia Graphics Drivers`

## List of Packages
This is a list of stuff that is configured by and or used in these files.

- x11-video-nvidiaG04
- alacritty
- python3-Jinja2
- python3-PyYAML
- python3
- python3-flake8
- python3-flake8-import-order
- python3-flake8-quotes
- python
- python-gtk
- xdg-utils
- gvim
- dunst
- conky
- fontawesome-font
- pulseaudio-utils
- xbacklight
- kdeconnect-cli
- curl
- rofi
- xsetroot
- lxsession
- parcellite
- compton
- gtk2-metatheme-adapta
- gtk3-metatheme-adapta
- breeze5-cursors
- papirus-icon-theme
- google-roboto-fonts
- libgnomesu
- bspwm
- sxhkd
- ctags
- pandoc
- ffmpeg
- i3lock
- feh

## npm packages:

- htmlhint
- typescript
- typescript-language-server

## python packages:

- python-language-server

## Third party programs

The following software should be downloaded and installed from third parties accoring to their guides:

- insync
- telegram

# Tipps, tricks, and fixes

these primarily apply to my specific OS and hardware (currently OpenSUSE tumbleween and Thinkpad P51).

## Thinkpad P51

### Scale 4k screen that doesn't accept a lower resolution

Add to `/etc/X11/xorg.conf.d/50-screen.conf`:

```
Section "Screen"
  Identifier "Default Screen"
  Device "Default Device"
  Monitor "Default Monitor"
  Option "metamodes" "DP-4: nvidia-auto-select +0+0 {viewportin=1920x1080, ForceCompositionPipeline=On}, DP-3.1: nvidia-auto-select +1920+0 {ForceCompositionPipeline=On}, DP-3.2: nvidia-auto-select +3840+0 {ForceCompositionPipeline=On}"

EndSection
```

### Scale lightdm on 4k screen

Create '/etc/lightdm/lightdm.conf'

```
[SeatDefaults]
greeter-setup-script=/etc/profile.d/lightdm.sh
session-setup-script=/etc/profile.d/lightdm.sh
```

and the respective `/etc/profile.d/lightdm.sh`:

```
#! /bin/sh
if [ ! -z "$DISPLAY" ]; then
        /usr/bin/xrandr --output DP-4 --scale 0.5x0.5
fi
```
Don't forget to `chmod +x` that one!

### Shut up the sound card beeping

Soundcard somehow beeps audibly when on powersave.
Adding the following line to `/etc/modprobe.d/50-alsa.conf` shuts it up:

```
options snd-hda-intel power_save=0
```

## OpenSUSE
### Install ruby gems

By default openSUSE messes with the name of installed gems, Use `gem install --no-format-executable` to prevent that. Also, `--user-install` can be helpful to install to a directory in the user's home.

## Firefox
### Fixing Firefox right-click menu on bspwm:

Apparently window borders fuck with the mouse positioning. This snipped in *userChrome* is a workaround:

```
#contentAreaContextMenu {
    margin: 10px 0 0 10px
}
```

### Firefox: prevent dark theme in web content

Set any light theme for the widget content in `about:config`:

```
widget.content.gtk-theme-override | string | Adapta
```

## General configuration

Most of these need super user rights and thus cannot be directly addressed by the dotfiles scripts.

### Correctly mount windows ntfs partitions in /etc/fstab

Use `nofail,x-systemd.device-timeout=1` to let the linux system boot even if windows kept it's grip on the partition. If 'fastboot' is disabled in Windows it will usually not do that, but one can never be sure.

Use `umask=077,uid=1000,gid=100`. One of those enables thunar to correctly handle the trash bin from directories on this partition. Maybe not all of these are needed, but all three together sure do the job.

Working example:

```
UUID=2C526336526303C4                      /mnt/daten              ntfs   defaults,umask=077,uid=1000,gid=100,user,rw,nofail,x-systemd.device-timeout=1  0  0
```

### LightDM GTK greeter:

The `main.py` scripts tries to set the lightdm configuration in `/etc/lightdm/lightdm-gtk-greeter.conf` but of course needs the permission to do so.

On a personal machine, `chmod 777 /etc/lightdm` does the trick.

Further more, it is necessary that lightdm can read the background image in question. Setting permissions for 'pthers' to 'read only' is sufficient.

## KDE Plasma on P51
### Fix kwin screen tearing on nvidia with triple buffering

Create a file called `/etc/profile.d/kwin.sh` with the following:

```
#!/bin/sh
export KWIN_TRIPLE_BUFFER=1
```
### Fix kwin screen tearing on nvidia with triple buffering

Create a file called `/etc/profile.d/kwin.sh` with the following:

```
#!/bin/sh
export KWIN_TRIPLE_BUFFER=1
```

