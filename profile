# Let Qt apps use gtk2 theme:
export QT_QPA_PLATFORMTHEME=gtk2

# Let xdg-su and therefore Yast use gnomesu if available:
export XDG_CURRENT_DESKTOP=LXDE

# Add ruby gems to path
export PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin:$HOME/.local/bin"
